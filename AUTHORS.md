# Contributors

For the time being pter is maintained by

 - Robert Labudda, <https://vonshednob.cc>

A big thank you to all contributors!

In no particular order:

 - Daniel
 - Jason
 - Gerrit
 - Marco
 - Andrea
 - bambirombi
 - Ulysse
 - lrustand
 - Fabian
 - jerseyblue
 - Nick
 - Pawel
 - maxigaz
 - onovy
 - andrei-a-papou
 - joeac

