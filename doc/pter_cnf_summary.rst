Aside from the data files in the todo.txt format (see `Conforming to`_),
pter's behaviour can be configured through one or more configuration files.

The configuration file's default location is at ``~/.config/pter/pter.conf``.

For details, please refer to `the help right here <doc/pter.config.rst>`_ or
the man page `pter.config(5) <man:pter.config(5)>`_.

