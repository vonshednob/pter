Getting Things Done
===================

With pter you can apply the Getting Things Done method to a single todo.txt
file by using context and project tags, avoiding multiple lists.

For example, you could have a ``@in`` context for the list of all tasks
that are new. Now you can just search for ``@in`` (and save it as a named search) to find all new tasks.

To see all tasks that are on your "Next task" list, a good start is to
search for "``done:no not:@in``" (and save this search query, too).

